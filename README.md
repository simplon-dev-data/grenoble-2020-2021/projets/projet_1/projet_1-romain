# Projet 1 : Mesure d'audience sur le web

> Exemple formateur Romain C.

## Pré-requis

- `docker`
- `docker-compose`
- `sqlite3`
- `pipenv`

## Installation

Pour installer l'environnement et les dépendances Python nécessaires:

```bash
pipenv install -d
```

Pour lancer Metabase:

```bash
docker-compose up -d
```

## Pipeline ETL

Le pipeline est composé de 3 étapes :

1. Récupération et extraction des logs au format Common Log Format
2. Parsing et enrichissement des logs avec le user-agent
3. Chargement des données propres dans une base SQLite

Pour exécuter le pipeline:

```bash
bash pipeline.sh
```

## Exploitation

### Requêtes SQL

Le dossier [`queries`](queries) contient des requêtes d'exploitation des données propres.

Par exemple, pour afficher les navigateurs (hors bots) les plus utilisés, par ordre décroissant:

```bash
sqlite3 .docker/metabase/analytics.db < queries/browsers.sql
```

### Metabase

Les requêtes d'exploitation peuvent être invoquées avec Metabase,
en ouvrant un navigateur à l'adresse [http://localhost:3000](http://localhost:3000).
