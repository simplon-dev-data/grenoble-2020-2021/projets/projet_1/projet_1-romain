#!/bin/sh

set -ex

SRC_URL=${1}
DATA_DIR=${2}
SRC_ZIP=${3}.zip

mkdir -p ${DATA_DIR}
curl -o ${SRC_ZIP} -L ${SRC_URL}
unzip ${SRC_ZIP} -d ${DATA_DIR}
