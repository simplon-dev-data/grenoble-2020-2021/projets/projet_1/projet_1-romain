DROP TABLE IF EXISTS access_logs_raw;

.mode csv
.separator ','
.import "data/access.log.csv" access_logs_raw