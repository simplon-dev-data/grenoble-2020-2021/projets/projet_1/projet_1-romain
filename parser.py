import csv
import datetime
import locale
import os
import re
import sys
import user_agents

from typing import Any, Dict, Optional


clf = re.compile(
    r"(?P<ip_address>\d+.\d+.\d+.\d+) "
    r"(?P<client_id>-) (?P<user_id>-) "
    r"\[(?P<timestamp>.*)\] "
    r"\"(?P<method>.{3,7}) (?P<resource>.*) (?P<protocol>.*)\" "
    r"(?P<status_code>\d{3}) (?P<response_size>\d+) "
    r"\"(?P<referer>.*)\" \"(?P<user_agent>.*)\" "
    r"\"(?P<extra>.*)\""
)

locale.setlocale(locale.LC_ALL, "en_US")


def parse_log_line(line: str) -> Optional[Dict[str, Any]]:
    result = clf.match(line)
    if result:
        return result.groupdict()

    return None


def parse_timestamp(data: Dict[str, Any]) -> None:
    data["timestamp"] = datetime.datetime.strptime(
        data["timestamp"], "%d/%b/%Y:%H:%M:%S %z"
    ).isoformat()


def find_user_agent(data: Dict[str, Any]) -> Dict[str, Any]:
    ua = user_agents.parse(data["user_agent"])
    return dict(
        browser_family=ua.browser.family,
        browser_version=ua.browser.version_string,
        os_family=ua.os.family,
        os_version=ua.os.version_string,
        device_family=ua.device.family,
        device_brand=ua.device.brand,
        device_model=ua.device.model,
        is_mobile=ua.is_mobile,
        is_tablet=ua.is_tablet,
        is_pc=ua.is_pc,
        is_touch_capable=ua.is_touch_capable,
        is_bot=ua.is_bot,
        is_email_client=ua.is_email_client,
    )


def parse_input():
    columns = [
        "ip_address",
        "client_id",
        "user_id",
        "timestamp",
        "method",
        "resource",
        "protocol",
        "status_code",
        "response_size",
        "referer",
        "user_agent",
        "extra",
        "browser_family",
        "browser_version",
        "os_family",
        "os_version",
        "device_family",
        "device_brand",
        "device_model",
        "is_mobile",
        "is_tablet",
        "is_pc",
        "is_touch_capable",
        "is_bot",
        "is_email_client",
    ]

    writer = csv.DictWriter(sys.stdout, extrasaction="ignore", fieldnames=columns)
    writer.writeheader()

    errors = 0
    count = 0

    for line in sys.stdin:
        count += 1
        parsed = parse_log_line(line)
        if parsed:
            parse_timestamp(parsed)
            ua = find_user_agent(parsed)
            parsed.update(ua)
            writer.writerow(parsed)
        else:
            errors += 1

    return count, errors


def dump_stats(count: int, errors: int) -> None:
    with open(os.path.join("data", "parser_stats.csv"), "w") as stats:
        w = csv.DictWriter(stats, fieldnames=["datetime", "count", "errors"])
        w.writeheader()
        w.writerow(
            dict(
                datetime=datetime.datetime.now().isoformat(), count=count, errors=errors
            )
        )


if __name__ == "__main__":
    count, errors = parse_input()
    dump_stats(count, errors)
