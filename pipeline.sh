#!/bin/bash
set -ex

DATA_DIR=data
WEBLOGS_URL=https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J
WEBLOGS_FILENAME=${DATA_DIR}/access.log
WEBLOGS_SAMPLE_FILENAME=${DATA_DIR}/access.log.sample
WEBLOGS_CSV_FILENAME=${DATA_DIR}/access.log.csv

##############################################################################
# Extraction des donnees sources
##############################################################################

if [[ ! -e ${WEBLOGS_FILENAME} ]]; then
bash extract.sh ${WEBLOGS_URL} ${DATA_DIR} ${WEBLOGS_FILENAME}
fi
head -n 100000 ${WEBLOGS_FILENAME} > ${WEBLOGS_SAMPLE_FILENAME}

##############################################################################
# Transformation des donnees brutes (parsing, enrichissement)
##############################################################################

python parser.py < ${WEBLOGS_SAMPLE_FILENAME} > ${WEBLOGS_CSV_FILENAME}

##############################################################################
# Chargement des donnees en base
##############################################################################

sqlite3 .docker/metabase/analytics.db < load.sql
sqlite3 .docker/metabase/analytics.db < transform.sql
