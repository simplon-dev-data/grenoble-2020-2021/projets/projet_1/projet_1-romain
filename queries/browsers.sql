-- Navigateurs (hors bots) les plus utilisés, par ordre décroissant
SELECT
    "access_logs"."browser_family" AS "browser",
    COUNT(*) AS "count"
FROM "access_logs"
WHERE "access_logs"."is_bot" = 'False'
GROUP BY "browser"
ORDER BY "count" DESC