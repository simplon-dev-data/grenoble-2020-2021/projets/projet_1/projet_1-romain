-- Nombre de clients distincts (par adresse IP, hors bots)
SELECT
    COUNT(distinct "access_logs"."ip_address") AS "clients"
FROM "access_logs"
WHERE "access_logs"."is_bot" = 'False';