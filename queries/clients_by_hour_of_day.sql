-- Nombre de clients distincts (par adresse IP, hors bots) par heure de la journée
SELECT
    strftime('%H', "access_logs"."timestamp") AS "hour_of_day",
    COUNT(distinct "access_logs"."ip_address") AS "clients"
FROM "access_logs"
WHERE "access_logs"."is_bot" = 'False'
GROUP BY "hour_of_day"
ORDER BY "hour_of_day" ASC;