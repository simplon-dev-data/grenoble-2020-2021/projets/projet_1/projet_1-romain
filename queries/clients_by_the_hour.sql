-- Nombre de clients distincts (par adresse IP, hors bots) par heure par jour
SELECT
    datetime(strftime('%Y-%m-%d %H:00', "access_logs"."timestamp")) AS "hour",
    COUNT(distinct "access_logs"."ip_address") AS "clients"
FROM "access_logs"
WHERE "access_logs"."is_bot" = 'False'
GROUP BY "hour"
ORDER BY "hour" DESC;