-- Appareils (hors bots) les plus utilisés, par ordre décroissant
SELECT
    "access_logs"."device_family" AS "device",
    COUNT(*) AS "count"
FROM "access_logs"
WHERE "access_logs"."is_bot" = 'False'
GROUP BY "device"
ORDER BY "count" DESC