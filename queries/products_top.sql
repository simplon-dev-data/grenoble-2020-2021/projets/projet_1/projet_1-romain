-- Les 10 produits les plus consultés
SELECT
    substr("access_logs"."resource", 1, 15) AS "product",
    COUNT(*) AS "count"
FROM "access_logs"
WHERE "access_logs"."resource" LIKE '/product/_____/%'
GROUP BY "access_logs"."resource"
ORDER BY "count" DESC
LIMIT 10;