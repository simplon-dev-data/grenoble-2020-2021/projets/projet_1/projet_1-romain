-- Systèmes (hors bots) les plus utilisés, par ordre décroissant
SELECT
    "access_logs"."os_family" AS "system",
    COUNT(*) AS "count"
FROM "access_logs"
WHERE "access_logs"."is_bot" = 'False'
GROUP BY "system"
ORDER BY "count" DESC