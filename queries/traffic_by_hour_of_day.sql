-- Traffic web par heure de la journée
SELECT
    strftime('%H', "access_logs"."timestamp") AS "hour_of_day",
    COUNT(*) AS "requests"
FROM "access_logs"
GROUP BY "hour_of_day"
ORDER BY "hour_of_day" ASC;