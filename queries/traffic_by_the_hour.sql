-- Traffic web par heure et par jour
SELECT
    datetime(strftime('%Y-%m-%d %H:00', "access_logs"."timestamp")) AS "hour",
    COUNT(*) AS "requests"
FROM "access_logs"
GROUP BY "hour"
ORDER BY "hour" DESC;