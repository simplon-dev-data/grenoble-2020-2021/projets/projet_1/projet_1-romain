DROP TABLE IF EXISTS access_logs;

CREATE TABLE access_logs AS
SELECT
    "ip_address",
    "user_id",
    "timestamp",
    "method",
    "resource",
    "protocol",
    CAST("status_code" AS INTEGER) AS "status_code",
    CAST("response_size" AS INTEGER) AS "response_size",
    "referer",
    "browser_family",
    "browser_version",
    "os_family",
    "os_version",
    "device_family",
    "device_brand",
    "device_model",
    "is_mobile",
    "is_tablet",
    "is_pc",
    "is_touch_capable",
    "is_bot",
    "is_email_client"
FROM access_logs_raw;